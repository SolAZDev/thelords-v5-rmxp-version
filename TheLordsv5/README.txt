=======================================
==== The Lords (v5.4.5) MB Release ====
=======================================
Things You Should know..


The Story :: 
 The Story itself is a bit complicated
to explain, I apologize. BUT, The reason
why it's complicated it's because, the 
story itself is also a spoiler....
The player slowly keeps getting clues
of the story actually, I'd kill it if
I told you. But I hope this demo can give
you a nice small idea...  More like a tiny
preview of the first piece of what the 
story is about. Regardless, I hope you enjoy
the demo.

The Game System ::
 As you can see, this game is build with
RPG Maker XP (1.02a) and has the adavnced
sub-engine known as Blizz-ABS (2.85), which
honestly helped the game's creation in endless
ways..

Development Team ??
 In this case, I did most of the work myself,
excluding most of the scripts and art. I did
all the scenes, maps, enemies, quests and what
not. However I do intend to make a future
release...

Future Release ??
 It's something I haven't released in a long 
time and it's done almost each mass update
(v0.9, V1.3, v2.5, v3.4, v4.2) It's called
"The Lords - Theory Disc" This would be the
5th instalment. Basically, it's a demo of the
game, but as close as how the real project is
meant to be. Thus, this game will probably get
a full remake in 3D.

Controls ::

D-PAD			 :: Move
Next/Prev Page		 :: Q/W
Comfirm			 :: Z, Space
Cancel/Menu		 :: X
Attack			 :: A
Skill			 :: S
Item			 :: F
Defend 			 :: D
Change Leader		 :: E
Run			 :: C
Sneak			 :: T
Jump			 :: (Missing)
Turn (Not Moving) 	 :: G
HUD & Hotkeys		 :: = & -
Minimap			 :: R
Skills/Items < Fast Use	 :: 1...9


